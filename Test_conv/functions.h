//
//  functions.h
//  Test_conv
//
//  Created by Pierre Ballesta on 01/07/2019.
//  Copyright © 2019 Pierre Ballesta. All rights reserved.
//

#ifndef functions_h
#define functions_h

#include <stdio.h>

// We define the Corr structure to be used with the function correlation.
struct Corr {
    double corr; // this is the correlation value.
    double amp; // this is the amplitude relative to the pattern amplitude.
};

// We define the divident, rest and quotient of an Euclidean division.
struct Divid {
    int dividend;
    int quotient;
    int remainder;
};

struct Divid AddDivid(struct Divid a, struct Divid b);
struct Divid CreaDivid(int a, int b);
float SubSqDivid(struct Divid a1, struct Divid a2, struct Divid a3);
struct Corr CorrInt(int arr_1[], int arr_2[], int length, int noise);
int PositionMax(double arr[],int lengthArr);
double StDevILong(int arr[],int lengthArr);
int MeanInt(int arr[],int lengthArr);
int I2Max(double arr[],const int lengthSize[],int lengthArr);

double Mean(double arr[],int lengthArr);
double StDev(double arr[],int lengthArr);

#endif /* functions_h */
