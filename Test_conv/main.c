//
//  main.c
//  Test_conv
//
//  Created by Pierre Ballesta on 16/05/2019.
//  Copyright © 2019 Pierre Ballesta. All rights reserved.
//

#include "functions.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>

int main (int argc, char *argv[])
{
    
    // the signal should consist of integers. Values must be evenly space in time.
    printf("Enter the signal file path: \n");
    char signal[256];
    scanf("%s",&signal);
    
    printf("Enter the pattern file path: \n");
    char standardStep[256];
    scanf("%s",&standardStep);
    
    // variables for the files reading
    printf("Enter the save file path: \n");
    char saveFile[256];
    scanf("%s",&saveFile);
    
    double precision;
    printf("Enter the precision level (between 0 and 1): \n");
    scanf("%lf",&precision);
    
    int detect;
    printf("Enter the detection level (integer): \n");
    scanf("%d",&detect);
    
    int noise;
    printf("Enter the noise standard deviation (integer): \n");
    scanf("%d",&noise);
    
    printf("%s \n", signal);
    printf("%s \n", standardStep);
    printf("%s \n", saveFile);
    
    
    
    // These variables are used to read the files.
    char tampon[BUFSIZ];
    int actuel = 0;
    int c;
    
    // variables for the interpolation.
    // Length of the standard step, this value must be integers in an ascending or descending order. A same value should not appear twice.
    const int lengthSingleStep[]={70,76,82,88,94,100,106,112,118,124,130};
    const int sizeOfLength = sizeof(lengthSingleStep)/sizeof(lengthSingleStep[0]);
    
    // Count the number of elements in the file.
    FILE *fichier = fopen(standardStep, "r");
    if(fichier == NULL ) {
        printf("Problem reading pattern file.\n");
        return 1;
    }
    int lsignal = 0;
    while ((c = fgetc (fichier)) != EOF) {
        if (c == ';' || c == '\n') {
            lsignal+=1;
            actuel = 0;
            memset (tampon, 0, sizeof tampon);
        } else {
            tampon[actuel++] = c;
        }
    }
    fclose (fichier);
    
    int lengthStep = lsignal;
    
    // We create the array, this is a double signal as the Standard step was created using spline so that linear interpolation could be performed during this program.
    double standardStep_A[lengthStep];
    fichier = fopen(standardStep, "r");
    lsignal = 0;
    while ((c = fgetc (fichier)) != EOF) {
        if (c == ';' || c == '\n') {
            standardStep_A[lsignal] = strtod (tampon, NULL);
            lsignal+=1;
            actuel = 0;
            memset (tampon, 0, sizeof tampon);
        } else {
            tampon[actuel++] = c;
        }
    }
    fclose (fichier);
    // We calculate its mean.
    double meanStep=Mean(standardStep_A,lengthStep);
    
    
    // Read the Signal and create an array. This is an array of integer.
    fichier = fopen(signal, "r");
    if(fichier == NULL ) {
        printf("Problem reading signal file.\n");
        return 1;
    }
    lsignal = 0;
    while ((c = fgetc (fichier)) != EOF) {
        if (c == ';' || c == '\n') {
            lsignal+= 1;
            actuel = 0;
            memset (tampon, 0, sizeof tampon);
        } else {
            tampon[actuel++] = c;
        }
    }
    fclose (fichier);
    
    int lengthSignal = lsignal;
    
    int signal_A[lengthSignal];
    fichier = fopen(signal, "r");
    lsignal = 0;
    while ((c = fgetc (fichier)) != EOF) {
        if (c == ';' || c == '\n') {
            signal_A[lsignal] = strtol (tampon, NULL, 10);
            lsignal+= 1;
            actuel = 0;
            memset (tampon, 0, sizeof tampon);
        } else {
            tampon[actuel++] = c;
        }
    }
    fclose (fichier);
    
    // We calculate the average to avoid extreme values.
    int meanSignal_A = MeanInt(signal_A, lengthSignal);
    
    // We remove the average to decrease the maximum values.
    
    for (int j=0; j<lengthStep; j++){
        standardStep_A[j]-=meanStep;
    }
    
    // We remove the average to decrease the maximum values obtained afterward.
    for (int j=0; j<lengthSignal; j++){
        signal_A[j]-= meanSignal_A;
    }
        
    // create the correlation 2D array.
    int interpStep[lengthSingleStep[sizeOfLength-1]];
    double corr2D[sizeOfLength][lengthSignal];
    // Variables are declared
    int k=0;
    float b, r, d;
    int ku = 0;
    int kd = 0;
    int l=0;
    int posTempStep[lengthSignal];
    int posTempUp[lengthSignal];
    int posTempDown[lengthSignal];
    int kk;
    // We create an array that only takes into account the values above the detection level .
    double aboveDetection[lengthSignal];
    // Count the number of points above the threshold.
    int aboveNumber[lengthSignal];
    
    for (int i=0; i<sizeOfLength; i++){
        // create the interpolated step.
        interpStep[0]=round(standardStep_A[0]);
        // the beginning and end values are the same.
        interpStep[lengthSingleStep[i]-1]=round(standardStep_A[lengthStep-1]);
        // Where are the points.
        k=0;
        for(int j=1; j<lengthSingleStep[i]-1; j++){
            while(k*(lengthSingleStep[i]-1)<j*(lengthStep-1)){
                k++;
            }
            b=j*(lengthStep-1)*(standardStep_A[k-1]-standardStep_A[k]);
            r=k*standardStep_A[k]-(k-1)*standardStep_A[k-1];
            d=lengthSingleStep[i]-1;
            // We want integers.
            interpStep[j]=round(b/d+r);
        }
        
        // We add 0s to the treated signal so that each correlation is the same length and can be compared.
        
        int signal_B[lengthSignal+lengthSingleStep[i]-1];
        for(int j=0;j<lengthSingleStep[i]/2;j++){
            signal_B[j]=0;
        }
        for(int j=lengthSingleStep[i]/2; j<lengthSingleStep[i]/2+lengthSignal; j++){
            signal_B[j]=signal_A[j-lengthSingleStep[i]/2];
        }
        for(int j=lengthSingleStep[i]/2+lengthSignal;
            j<lengthSignal+lengthSingleStep[i]-1; j++){
            signal_B[j]=0;
        }
        
        // calculate the correlated function.
        int length = lengthSingleStep[i];
        struct Divid a1={length,0,0}, a2={length,0,0}, a3={length,0,0}, a4={length,0,0}, a5={length,0,0};
        double c4=0,c5=0, sq, ld;
        for(int j=0; j<length; j++){
            a1=AddDivid(a1, CreaDivid(interpStep[j]*signal_B[j], length));
            a2=AddDivid(a2, CreaDivid(interpStep[j], length));
            a3=AddDivid(a3, CreaDivid(signal_B[j], length));
            a4=AddDivid(a4, CreaDivid(interpStep[j]*interpStep[j], length));
            a5=AddDivid(a5, CreaDivid(signal_B[j]*signal_B[j], length));
        }
        // We normalize by the standard deviation and move to float values.
        ld = SubSqDivid(a1,a2,a3);
        c4 = SubSqDivid(a4,a2,a2);
        c5 = SubSqDivid(a5,a3,a3)-noise*noise;
        if (sqrt(c5)>=detect){
            corr2D[i][0] = ld/sqrt(c4)/sqrt(c5);
        }
        else{
            corr2D[i][0] = 0;
        }
        for(int j=0; j<lengthSignal-2; j++){
            int jj=j+length;
            struct Divid a1={length,0,0};
            a3=AddDivid(a3, CreaDivid(signal_B[jj]-signal_B[j], length));
            a5=AddDivid(a5, CreaDivid(signal_B[jj]*signal_B[jj]-signal_B[j]*signal_B[j], length));
            c5 = SubSqDivid(a5,a3,a3)-noise*noise;
            for(int la=0; la<length; la++){
                a1=AddDivid(a1, CreaDivid(interpStep[la]*signal_B[j+1+la], length));
            }
            if (sqrt(c5)>=detect){
                sq=sqrt(c4)*sqrt(c5);
                ld = SubSqDivid(a1,a2,a3);
                // warning in case of problem.
                if(isnan(ld/sq)){
                    printf("Alert! Alert! Divide by zero0o0os.\n");
                    return 1;
                }
                corr2D[i][j+1] = ld/sqrt(c4)/sqrt(c5);
            }
            else{
                corr2D[i][j+1] = 0;
            }
         }
    }
    //We now have a 2D matrices recording the correlation values. We are looking at the maximum.
    // We now look for local maxima of values close to 1.
    ku = 0;
    kd = 0;
    
    // We create an array that only takes into account the values above the detection level .
    // We initiate them at 0.
    for (int i=0; i<lengthSignal; i++){
        aboveDetection[i]=0;
        aboveNumber[i]=0;
    }
    // average over the points above the threshold.
    // We look for part of the signal verifiing: above detection, length 3 or longer. This gives us the plages over which we study our signal to find the image of the Standard Step.
    for (int j=0; j<lengthSignal; j++){
        for (int i=0; i<sizeOfLength; i++){
            if(corr2D[i][j]>=precision){
                aboveDetection[j]+=corr2D[i][j];
                aboveNumber[j]+=1;
            }
        }
        if(aboveNumber[j]>2){
            aboveDetection[j]=(double)aboveDetection[j]/aboveNumber[j];
        }
    }
    
    l=0;
    kk = 0;
    while(kk<lengthSignal-1){
        while(kd-ku<2 && kk<lengthSignal-1){
            if(aboveDetection[kk]<precision && aboveDetection[kk+1]>precision){
                ku=kk+1;
            }
            else if(aboveDetection[kk]>precision && aboveDetection[kk+1]<precision){
                kd=kk;
            }
            kk++;
        }
        if(kk<lengthSignal-1){
            double op[kd-ku+1];
            for(int ii=0;ii<kd-ku+1;ii++){
                op[ii]=aboveDetection[ku+ii];
            }
            posTempStep[l]=ku+PositionMax(op,kd-ku+1);
            posTempDown[l]=kd;
            posTempUp[l]=ku;
            kd=0;
            ku=0;
            l++;
        }
    }
    int posStep[l];
    int posUp[l];
    int posDown[l];
    
    for(int j=0; j<l; j++){
        posStep[j]=posTempStep[j];
        posUp[j]=posTempUp[j];
        posDown[j]=posTempDown[j];
    }
    
    // We look for the position of the maxima in each plage.
    int lengthOfTheBestStep[l];
    double validityOfTheBestStep[l];
    double amplitudeOfTheBestStep[l];
    
    double arr[sizeOfLength];
    for(int j=0; j<l; j++){
        for(int i=0; i<sizeOfLength; i++){
            arr[i]=corr2D[i][posStep[j]];
        }
        
        // we get the maximum position
        int posTemp=PositionMax(arr,sizeOfLength);
        double validitytemp=arr[posTemp];
        int lengthOfTheBestStepTemp=lengthSingleStep[posTemp];
        double amplTemp;
        
        // we use a 2nd order fit to get a position better than the size of the step.
        lengthOfTheBestStep[j]=I2Max(arr,lengthSingleStep,sizeOfLength);
        //printf("%d\n",lengthOfTheBestStep[j]);
        // we calculate the validity of the fit at this point:
        
        // We calculate the interpolated step value:
        int interpStep[lengthOfTheBestStep[j]];
        interpStep[0]=round(standardStep_A[0]);
        // the beginning and end values are the same.
        interpStep[lengthOfTheBestStep[j]-1]=round(standardStep_A[lengthStep-1]);
        // Where are the points.
        k=0;
        for(int i=1; i<lengthOfTheBestStep[j]-1; i++){
            while(k*(lengthOfTheBestStep[j]-1)<i*(lengthStep-1)){
                k++;
            }
            b=i*(lengthStep-1)*(standardStep_A[k-1]-standardStep_A[k]);
            r=k*standardStep_A[k]-(k-1)*standardStep_A[k-1];
            d=lengthOfTheBestStep[j]-1;
            // We want integers.
            interpStep[i]=round(b/d+r);
        }
        
        // We calculate the correlation at the 0 order estimation:
        int signal_C[lengthOfTheBestStepTemp];
        for(int i=0; i<lengthOfTheBestStepTemp;i++){
            if(i+posTemp-lengthOfTheBestStepTemp/2>=0 && i+posTemp-lengthOfTheBestStepTemp/2<lengthSignal){
                signal_C[i]=signal_A[i+posTemp-lengthOfTheBestStepTemp/2];}
            else{
                signal_C[i]=0;;
                }
        }
        struct Corr intermediateCorr = CorrInt(signal_C,interpStep,lengthOfTheBestStep[j],noise);
        amplTemp = intermediateCorr.amp;
        
        int signal_B[lengthSignal+lengthOfTheBestStep[j]-1];
        for(int i = 0;i<lengthOfTheBestStep[j]/2;i++){
            signal_B[i]=0;
        }
        for(int i = lengthOfTheBestStep[j]/2; i < lengthOfTheBestStep[j]/2+lengthSignal; i++){
            signal_B[i]=signal_A[i-lengthOfTheBestStep[j]/2];
        }
        for(int i=lengthOfTheBestStep[j]/2+lengthSignal;
            i<lengthSignal+lengthOfTheBestStep[j]-1; i++){
            signal_B[i]=0;
        }
        
        //We check on a small segment around the estimated best position that we effectively have the maximum.
        int lengthVerif=11;
        double tempCorr[lengthVerif];
        double tempAmp[lengthVerif];
        
        for (int ii=0; ii<lengthVerif; ii++){
            
            int signal_C[lengthOfTheBestStep[j]];
            for(int i=0; i<lengthOfTheBestStep[j];i++){
                if(i+posStep[j]-lengthOfTheBestStep[j]/2-lengthVerif/2+ii>=0 && i+posStep[j]-lengthOfTheBestStep[j]/2 -lengthVerif/2+ii<lengthSignal){
                    signal_C[i] = signal_A[i+posStep[j]-lengthOfTheBestStep[j]/2 - lengthVerif/2+ii];}
                else{
                    signal_C[i]=0;
                }
            }
            struct Corr intermediateCorr = CorrInt(signal_C, interpStep, lengthOfTheBestStep[j],noise);
            tempCorr[ii] = intermediateCorr.corr;
            tempAmp[ii] = intermediateCorr.amp;
        }
        //int poss=posStep[j]-lengthVerif/2+PositionMax(tempCorr,lengthVerif);
        int poss = PositionMax(tempCorr,lengthVerif);
        validityOfTheBestStep[j] = tempCorr[poss];
        amplitudeOfTheBestStep[j] = tempAmp[poss];
        if (validityOfTheBestStep[j]<validitytemp){
            validityOfTheBestStep[j] = validitytemp;
            lengthOfTheBestStep[j] = lengthOfTheBestStepTemp;
            amplitudeOfTheBestStep[j] = amplTemp;
        }
        else{
            posStep[j] = posStep[j]-lengthVerif/2+poss;
        }
    }
    
    // We save the position of the best fit and the length of the step.
    fichier = fopen(saveFile, "w");
    fprintf(fichier, "step nbr, position, length, amplitude, validity, standard deviation\n");
    
    for(int i = 0; i < l; i++){
        double ecarttype = sqrt(2*(1-validityOfTheBestStep[i]));
        fprintf(fichier, "%d,%d,%d,%f,%f,%f\n", i, posStep[i], lengthOfTheBestStep[i], amplitudeOfTheBestStep[i], validityOfTheBestStep[i], ecarttype);
    }
    fclose(fichier);
    return 0;
}
