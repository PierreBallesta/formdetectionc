//
//  functions.c
//  Test_conv
//
//  Created by Pierre Ballesta on 01/07/2019.
//  Copyright © 2019 Pierre Ballesta. All rights reserved.
//

#include "functions.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>


// We first define equations for integer values of the signals.

// This is a function to add two Divid structure.
struct Divid AddDivid(struct Divid a, struct Divid b){
    // If two Divid have different dividents they cannot be summed.
    if(a.dividend != b.dividend) // You cannot add two Divids with different dividents.
        exit(-1);
    struct Divid f;
    f.dividend=a.dividend;
    f.quotient=a.quotient+b.quotient+(a.remainder+b.remainder)/f.dividend;
    f.remainder=(a.remainder+b.remainder)%f.dividend;
    return f;
}

// This is a function to create a Divid structure from two integers.
struct Divid CreaDivid(int a, int b){
    struct Divid f;
    f.dividend=b; // The second entry is the divident.
    f.quotient=a/b;
    f.remainder=a%b;
    return f;
}

// This is a recurring calculation so we put it in its own function. Roughly speaking it is used to removed the product of two Divids to a Divids and return a float. We use it to calculate the standard deviation and the cross product.
float SubSqDivid(struct Divid a1, struct Divid a2, struct Divid a3){
    if(a1.dividend != a2.dividend | a2.dividend != a3.dividend)
        exit(-1); // You cannot add two Divids with different dividents.
    float f = a1.quotient - a2.quotient * a3.quotient + (a1.remainder - a2.remainder * a3.quotient - a3.remainder*a2.quotient - (float)a2.remainder*a3.remainder/a3.dividend )/a3.dividend;
    return f;
}

// arr_1 and arr_2 must be vectors of same length. arr_1 is the studied array, arr_2 is the reference array. To prevent error ideally the average value is removed from arr_1 and arr_2. This correlation works with integers rather than doubles for as long as possible.
struct Corr CorrInt(int arr_1[], int arr_2[], int length, int noise){
    struct Corr results;
    struct Divid a1={length,0,0}, a2={length,0,0}, a3={length,0,0}, a4={length,0,0}, a5={length,0,0};
    float c4, c5, ld;
    for(int j=0; j<length; j++){
        a1=AddDivid(a1, CreaDivid(arr_1[j]*arr_2[j], length));
        a2=AddDivid(a2, CreaDivid(arr_1[j], length));
        a3=AddDivid(a3, CreaDivid(arr_2[j], length));
        a4=AddDivid(a4, CreaDivid(arr_1[j]*arr_1[j], length));
        a5=AddDivid(a5, CreaDivid(arr_2[j]*arr_2[j], length));
    }
    // We normalize by the standard deviation and move to float values.
    ld = SubSqDivid(a1,a2,a3);
    c4 = SubSqDivid(a4,a2,a2);
    c5 = SubSqDivid(a5,a3,a3)-noise*noise;
    if(c5>0){
        results.corr =ld/sqrt(c4)/sqrt(c5);
        results.amp=sqrt(c4/c5);
    }
    else{
        results.corr =0;
        results.amp=0;
    }
    return results;
}


// we define the Max function. Look for the position of the Maximum;
int PositionMax(double arr[],int lengthArr){
    double x=arr[0];
    int j=0;
    for(int i=1; i<lengthArr; i++){
        if(x<arr[i]){
            x=arr[i];
            j=i;
        }
    }
    return j;
}

// Standard deviation for long arrays of integers. That returns a double.
double StDevILong(int arr[],int lengthArr){
    struct Divid a1={lengthArr,0,0}, a2={lengthArr,0,0};
    double z;
    for(int i=1; i<lengthArr; i++){
        a1=AddDivid(a1, CreaDivid(arr[i]*arr[i], lengthArr));
        a2=AddDivid(a2, CreaDivid(arr[i], lengthArr));
    }
    z = SubSqDivid(a1,a2,a2);
    return sqrt(z);
}


// we define the mean function for arrays of integers.
int MeanInt(int arr[],int lengthArr){
    struct Divid a1={lengthArr,0,0};
    for(int i=1; i<lengthArr; i++){
        a1=AddDivid(a1, CreaDivid(arr[i], lengthArr));
    }
    int x = a1.quotient;
    return x;
}

// we define the I2Max function. Look for the position of the Maximum with a parabolic fit.
int I2Max(double arr[],const int lengthSize[],int lengthArr){
    int j=PositionMax(arr,lengthArr);
    if (j==0){j=lengthSize[0];}
    else if (j==lengthArr-1){j=lengthSize[lengthArr-1];}
    else {
        j  = ((lengthSize[j] + lengthSize[j+1])*(lengthSize[j]-lengthSize[j+1])*(arr[j-1]-arr[j]) -(lengthSize[j-1]+lengthSize[j])*(lengthSize[j-1]-lengthSize[j])*(arr[j]-arr[j+1])) /( (lengthSize[j]-lengthSize[j+1])*(arr[j-1]-arr[j]) -(lengthSize[j-1]-lengthSize[j])*(arr[j]-arr[j+1]) )/2;
    }
    return j;
}

// Later we can here define the fuctions for floats if needed.

// we define the mean function;
double Mean(double arr[],int lengthArr){
    double x=0;
    for(int i=1; i<lengthArr; i++){
        x+=arr[i];
    }
    x=x/lengthArr;
    return x;
}

// we define the Standard deviation function;
double StDev(double arr[],int lengthArr){
    double x=0;
    double y=0;
    double l=(double)lengthArr;
    for(int i=1; i<lengthArr; i++){
        x+=arr[i];
        y+=arr[i]*arr[i];
    }
    return sqrt(l*y-x*x)/l;
}

