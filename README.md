# FormDetectionC

This code is a C-port of the Matlab FormDetection program.

## Changes

The main changes are as follow:  
1. While Matlab uses FFT to compute convolutions, the C version uses a straightforward calculation for simplicity. 
2. The code works with signals composed of integers rather than floats as do Matlab.  
3. The averaging on the edge of the signal is slightly different. However, this change should not impact the detection. Roughly signal at the edges should be ignored.  
4. No graphic verification is present in this program. One could develop their own.  

## Instructions

The program will begin with the following prompt:  
```
Enter the signal file path: 
|  
```  
Enter the name of the folder containing the signal.

The next prompt asks you for the path to the pattern:  
```
Enter the pattern file path: 
|  
```  
The last path to be asked for is the file where you want the results to be saved:  
```
Enter the save file path: 
|  
``` 
The program will now ask for some numerical values.
```
Enter the precision level (between 0 and 1):
| 
```  
This will ask at what value do you consider the correlation to be good enough. This value is generally lower than the final one, but is used to get a better estimation of the position of the maximum.  
The program then asks for the detection level:
```
Enter the detection level (integer): 
|
```  
This value must be an integer, it's the standard deviation of the signal in the window of interest above which we consider that the signal is non stationary.  
Finally, program asks for the noise level:
```
Enter the noise standard deviation (integer): 
|
``` 
This is the standard deviation of the noise, generally obtained from an experiment at rest. If you don't know but the signal is clear you can put 0.


## Results
The program returns a .csv file which looks as follow:  

| step nbr | position | length | amplitude | validity | standard deviation |
|----------|:-------------:|:-------------:|:-------------:|:-------------:|------:|
| 1	| 474		| 101	| 	0.933634	| 0.966169	| 0.260118
| 2	| 611		| 97	| 	1.015612	| 0.973638	| 0.229615  
| 3	| 747		| 99	| 	0.982997	| 0.993584	| 0.113277  
| 4	| 885		| 100	| 	0.193078	| 0.989470	| 0.145120  
| 5	| 1022		| 104	| 	0.977179	| 0.991811	| 0.127980  
| 6	| 1160		| 97	| 	1.026892	| 0.961976	| 0.275769  
| 7	| 1300		| 103	| 	1.029344	| 0.996295	| 0.086078  
| 8	| 1438		| 104	| 	1.046037	| 0.992298	| 0.124111  
| 9	| 1576		| 93	| 	1.040473	| 0.936751	| 0.355667  
| 10| 1716	| 98	| 	1.021332	| 0.961535	| 0.277362  

The first column is the step number, the second is the position of the _middle_ of the pattern, length is the best number of time steps to adjust the pattern to the signal, amplitude is the high of the detected pattern over the model pattern, validity say 
 how good the fit is (the closer to 1 the better), standard deviation is another estimation of the validity of the fit (the smaller the better).

## Example

Here is an example of the detection of a step in the recording of an accelerometer during a short sprint. This visualisation is done with Matlab, using results from the program in C. The straight line is the raw signal, while the circles are the pattern as detected by the program. For a more complete description, one can check the FormDetection README and Math files at <https://bitbucket.org/PierreBallesta/formdetection>.


![Exemple](Test_conv/image/fig_C.png?raw=true)
_Figure 1 Signal superimposed with the patterns. The colours make successive patterns easier to visualize._

## Authors

* **Pierre Ballesta** -  [PierreBallesta](https://bitbucket.org/PierreBallesta)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.
